package main

import (
	"WebApp/internal/config"
	"WebApp/internal/lissajous/adapter/in"
	"WebApp/internal/lissajous/adapter/out"
	"WebApp/internal/lissajous/model"
	"WebApp/internal/lissajous/service"
	"WebApp/internal/rest"
	"context"
	"flag"
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"github.com/heetch/confita"
	"github.com/heetch/confita/backend/env"
	"github.com/heetch/confita/backend/file"
	"log"
	"net/http"
	"os"
	"strconv"
)

var rootPath string

func init() {
	flag.StringVar(&rootPath, "rootPath", "", "root folder")
	flag.Parse()
}

func main() {
	logger := log.New(os.Stdout, "", 0)

	cfg, err := setupCfg()
	if err != nil {
		logger.Fatal(err)
	}

	db := pg.Connect(&pg.Options{
		User:     cfg.Postgres.User,
		Password: cfg.Postgres.Password,
		Database: cfg.Postgres.Database,
		Addr:     cfg.Postgres.Host + ":" + strconv.Itoa(cfg.Postgres.Port),
	})

	err = createSchema(db)
	if err != nil {
		logger.Fatal(err)
	}

	infoStorage := out.NewInfoStorage(db, logger)
	infoHTTP := in.NewInfoHTTP(infoStorage)
	lissajousGet := in.NewHTTPGet(service.NewGetHandler(), infoStorage)

	router := rest.Handler(lissajousGet, infoHTTP)

	fmt.Println("Starting server on port 8080.....")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func createSchema(db *pg.DB) error {
	models := []interface{}{
		(*model.Info)(nil),
	}

	for _, m := range models {
		err := db.Model(m).CreateTable(&orm.CreateTableOptions{
			Temp:        false,
			IfNotExists: true,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func setupCfg() (config.Config, error) {
	ctx := context.Background()

	var cfg config.Config

	err := confita.NewLoader(
		file.NewBackend(fmt.Sprintf("%s/deploy/default.yaml", rootPath)),
		env.NewBackend(),
	).Load(ctx, &cfg)

	if err != nil {
		return config.Config{}, err
	}

	envDocker := os.Getenv("ENV")
	if envDocker != "docker" {
		cfg.Postgres.Host = "localhost"
	}

	return cfg, nil
}
