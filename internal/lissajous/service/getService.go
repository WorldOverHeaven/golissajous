package service

import (
	"WebApp/internal/lissajous/model"
	"fmt"
	"image"
	"image/gif"
	"math"
	"math/rand"
	"time"
)

const (
	whitelndex = 0 // Первый цвет палитры
	blacklndex = 1 // Следующий цвет палитры

	cycles  = 5     // Количество полных колебаний x
	res     = 0.001 // Угловое разрешение
	size    = 250   // Канва изображения охватывает [size..+size]
	nframes = 100   // Количество кадров анимации
	delay   = 8     // Задержка между кадрами (единица - 10мс)
)

type LissajousParam struct {
	Cycles  float64
	Res     float64
	Freq    float64
	Size    int
	Nframes int
	Delay   int
}

var _ GetHandler = (*getHandler)(nil)

type getHandler struct {
}

type GetHandler interface {
	Handle(param *LissajousParam) *gif.GIF
}

func NewGetHandler() GetHandler {
	return &getHandler{}
}

func (*getHandler) validate(param *LissajousParam) {
	if param.Cycles <= 0 || param.Cycles > 100 {
		param.Cycles = cycles
	}
	if param.Res <= 0.0005 || param.Res > 0.005 {
		param.Res = res
	}
	if param.Size <= 100 || param.Size > 500 {
		param.Size = size
	}
	if param.Nframes <= 0 || param.Nframes > 200 {
		param.Nframes = nframes
	}
	if param.Delay <= 0 || param.Delay > 100 {
		param.Delay = delay
	}
	if param.Freq <= 0 || param.Freq > 100 {
		param.Freq = rand.Float64() * 3.0
	}

	fmt.Printf("get param: %+v \n", param)
}

func (h *getHandler) Handle(param *LissajousParam) *gif.GIF {
	h.validate(param)

	param.Res = param.Res * math.Pi
	rand.Seed(time.Now().UTC().UnixNano())
	freq := param.Freq
	anim := gif.GIF{LoopCount: param.Nframes}
	phase := 0.0
	for i := 0; i < param.Nframes; i++ {
		rect := image.Rect(0, 0, 2*param.Size+1, 2*param.Size+1)
		img := image.NewPaletted(rect, model.Palette)
		for t := 0.0; t < param.Cycles*2*math.Pi; t += param.Res {
			x := math.Sin(t)
			y := math.Sin(t*freq + phase)
			img.SetColorIndex(param.Size+int(x*float64(param.Size)+0.5), param.Size+int(y*float64(param.Size)+0.5),
				blacklndex)
		}
		phase += 0.02 * math.Pi
		anim.Delay = append(anim.Delay, param.Delay)
		anim.Image = append(anim.Image, img)
	}
	return &anim
}
