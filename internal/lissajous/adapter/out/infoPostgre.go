package out

import (
	"WebApp/internal/lissajous/model"
	"github.com/go-pg/pg/v10"
	"log"
	"time"
)

var _ InfoStorage = (*infoStorage)(nil)

type infoStorage struct {
	db  *pg.DB
	log *log.Logger
}

type InfoStorage interface {
	GetById(id int64) *model.Info
	Create(ip string, time time.Time) error
	GetAll() ([]*model.Info, error)
	Delete(info *model.Info) error
}

func NewInfoStorage(db *pg.DB, log *log.Logger) InfoStorage {
	return &infoStorage{db: db, log: log}
}

func (i *infoStorage) GetById(id int64) *model.Info {
	return nil
}

func (i *infoStorage) Create(ip string, time time.Time) error {
	_, err := i.db.Model(&model.Info{
		IP:   ip,
		Time: time,
	}).Insert()
	if err != nil {
		i.log.Print(err)
		return err
	}
	return nil
}

func (i *infoStorage) GetAll() ([]*model.Info, error) {
	var infos []*model.Info
	err := i.db.Model(&infos).Select()
	if err != nil {
		i.log.Print(err)
		return nil, err
	}
	return infos, nil
}

func (i *infoStorage) Delete(info *model.Info) error {
	return nil
}
