package in

import (
	"WebApp/internal/lissajous/adapter/out"
	"WebApp/internal/lissajous/service"
	"fmt"
	"image/gif"
	"net/http"
	"strconv"
	"time"
)

type httpGet struct {
	handler service.GetHandler
	storage out.InfoStorage
}

type HTTPGet interface {
	Handle(w http.ResponseWriter, r *http.Request)
}

func NewHTTPGet(handler service.GetHandler, storage out.InfoStorage) HTTPGet {
	return &httpGet{handler: handler, storage: storage}
}

func (h *httpGet) Handle(w http.ResponseWriter, r *http.Request) {
	lissajousParam, err := h.extractParamsFromURL(r)
	if err != nil {
		fmt.Println("Error ", err)
		return
	}

	resGIF := h.handler.Handle(lissajousParam)
	err = gif.EncodeAll(w, resGIF)
	if err != nil {
		fmt.Println("Error ", err)
		return
	}

	ip := r.Header.Get("X-Forwarded-For")
	err = h.storage.Create(ip, time.Now())
	if err != nil {
		return
	}
}

func (*httpGet) extractParamsFromURL(r *http.Request) (*service.LissajousParam, error) {
	param := &service.LissajousParam{}

	extractParamFloat64(&param.Cycles, "cycles", r)
	extractParamFloat64(&param.Res, "res", r)
	extractParamInt64(&param.Size, "size", r)
	extractParamInt64(&param.Nframes, "nframes", r)
	extractParamInt64(&param.Delay, "delay", r)
	extractParamFloat64(&param.Freq, "freq", r)

	// fmt.Printf("get param: %+v \n", param)

	return param, nil
}

func extractParamInt64(i *int, paramName string, r *http.Request) {
	paramString := r.FormValue(paramName)
	param, err := strconv.ParseInt(paramString, 10, 32)
	if err != nil {
		//return nil, fmt.Errorf("cant get cycles from request: %w", err)
		*i = -1
	}
	*i = int(param)
}

func extractParamFloat64(f *float64, paramName string, r *http.Request) {
	paramString := r.FormValue(paramName)
	param, err := strconv.ParseFloat(paramString, 64)
	if err != nil {
		//fmt.Printf("cant get freq from request: %w \n", err)
		*f = -1
	}
	*f = param
}
