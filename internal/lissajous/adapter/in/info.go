package in

import (
	"WebApp/internal/lissajous/adapter/out"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

var _ InfoHTTP = (*infoHTTP)(nil)

type infoHTTP struct {
	i out.InfoStorage
}

type InfoHTTP interface {
	CreateHandler(w http.ResponseWriter, r *http.Request)
	GetAllHandler(w http.ResponseWriter, r *http.Request)
}

func NewInfoHTTP(i out.InfoStorage) InfoHTTP {
	return &infoHTTP{i: i}
}

func (i *infoHTTP) CreateHandler(w http.ResponseWriter, r *http.Request) {
	ip := r.Header.Get("X-Forwarded-For")
	err := i.i.Create(ip, time.Now())
	if err != nil {
		return
	}
	_, err = fmt.Fprintf(w, "Created!")
	if err != nil {
		return
	}
}

func (i *infoHTTP) GetAllHandler(w http.ResponseWriter, r *http.Request) {
	data, err := i.i.GetAll()
	if err != nil {
		return
	}
	for k := 0; k < len(data); k++ {
		fmt.Fprintf(w, "id: "+strconv.FormatInt(data[k].ID, 10)+"\t   time: "+data[k].Time.String()+"\t   ip: "+data[k].IP+"\n")
	}
}

//
//func (i *InfoHTTP) GetById(id int64) *model.Info {
//	return i.i.GetById(id)
//}

//
//func (i *InfoHTTP) Delete(info *model.Info) error {
//	return i.i.Delete(info)
//}
