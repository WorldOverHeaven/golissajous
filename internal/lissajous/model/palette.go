package model

import "image/color"

var Palette = []color.Color{
	color.RGBA{
		R: 0,
		G: 255,
		B: 0,
		A: 255,
	},
	color.Black,
}
