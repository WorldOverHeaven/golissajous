package model

import "time"

type Info struct {
	ID   int64
	IP   string
	Time time.Time
}
