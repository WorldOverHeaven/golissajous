package rest

import (
	"WebApp/internal/lissajous/adapter/in"
	"fmt"
	"github.com/go-chi/chi"
	"html/template"
	"io"
	"net/http"
)

func Handler(lissajousGet in.HTTPGet, infoStorage in.InfoHTTP) *chi.Mux {
	router := chi.NewRouter()

	router.Route("/", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			HandlerIndex(w)
		})
	})

	router.Route("/l", func(r chi.Router) {
		r.Get("/", lissajousGet.Handle)
	})

	router.Route("/data", func(r chi.Router) {
		r.Get("/create", infoStorage.CreateHandler)
		r.Get("/select", infoStorage.GetAllHandler)
	})

	return router
}

func HandlerIndex(out io.Writer) {
	tmpl, err := template.ParseFiles("internal/view/index.html")
	if err != nil {
		fmt.Printf("tmpl parse error %e", err)
		return
	}
	err = tmpl.Execute(out, "")
	if err != nil {
		return
	}
}
