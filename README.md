# GoLissajous

### Инструкция по запуску
1. Перейти в директорию deploy  
   cd deploy
2. Запустить docker-compose  
   docker-compose up

### Пример работы
Приложение позволят генерировать фигуры Лиссажу по заданным параметрам  
![img.png](img.png)
![img_1.png](img_1.png)  
